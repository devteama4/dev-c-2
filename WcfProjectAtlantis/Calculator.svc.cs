﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfProjectAtlantis
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "Calculator" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez Calculator.svc ou Calculator.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class Calculator : ICalculator
    {
        private MySqlConnection connection;
        private MySqlDataReader reader;

        public Calculator()
        {
            this.InitConnexion();
        }

        private void InitConnexion()
        {
            string connectionString = "SERVER=localhost;DATABASE=dbprojectatlantis;UID=root;PASSWORD=DBPWD;SslMode=None";
            this.connection = new MySqlConnection(connectionString);
        }

        public List<Data> GetData(string typedevice)
        {
            List<Data> datas = new List<Data>();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT date_data, value_data, unit_data, type_device FROM data INNER JOIN device ON data.id_device = device.id_device WHERE type_device = @type_device";
                cmd.Parameters.AddWithValue("@type_device", typedevice);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Data data = new Data()
                    {
                        Date_data = reader[0].ToString(),
                        Value_data = reader[1].ToString(),
                        Unit_data = reader[2].ToString(),
                        Type_device = reader[3].ToString()
                    };
                    datas.Add(data);
                }


                return datas;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }

        public List<Data> GetDataDeviceDate(string typedevice, string datedata)
        {
            List<Data> datas = new List<Data>();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT date_data, value_data, unit_data, type_device FROM data INNER JOIN device ON data.id_device = device.id_device WHERE type_device = @type_device AND date_data = @date_data";
                cmd.Parameters.AddWithValue("@type_device", typedevice);
                cmd.Parameters.AddWithValue("@date_data", datedata);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Data data = new Data()
                    {
                        Date_data = reader[0].ToString(),
                        Value_data = reader[1].ToString(),
                        Unit_data = reader[2].ToString(),
                        Type_device = reader[3].ToString()
                    };
                    datas.Add(data);
                }


                return datas;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }

        public List<Datas> GetDataAll()
        {
            List<Datas> datass = new List<Datas>();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM data";

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Datas datas = new Datas()
                    {
                        Id_data = reader[0].ToString(),
                        Date_data = reader[1].ToString(),
                        Value_data = reader[2].ToString(),
                        Unit_data = reader[3].ToString(),
                        Id_device = reader[4].ToString()
                    };
                    datass.Add(datas);
                }


                return datass;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }
        public Device GetDataSum(string d)
        {
            Device device = new Device();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT type_device,SUM(value_data) AS value_TT FROM data RIGHT JOIN device ON data.id_device = device.id_device WHERE type_device = @type_device";
                cmd.Parameters.AddWithValue("@type_device", d);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    device.Type_device = reader[0].ToString();
                    device.Value_TT = reader[1].ToString();
                }


                return device;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }
        public List<Datas> GetDataBetDate(string firstdate, string seconddate)
        {
            List<Datas> datass = new List<Datas>();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM data WHERE date_data BETWEEN @firstdate AND @seconddate";
                cmd.Parameters.AddWithValue("@firstdate", firstdate);
                cmd.Parameters.AddWithValue("@seconddate", seconddate);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Datas datas = new Datas()
                    {
                        Id_data = reader[0].ToString(),
                        Date_data = reader[1].ToString(),
                        Value_data = reader[2].ToString(),
                        Unit_data = reader[3].ToString(),
                        Id_device = reader[4].ToString()
                    };
                    datass.Add(datas);
                }


                return datass;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }

    }
}
