﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfProjectAtlantis
{
    [DataContract]
    public class Data
    {
        [DataMember]
        public string Date_data { get; set; }
        [DataMember]
        public string Value_data { get; set; }
        [DataMember]
        public string Unit_data { get; set; }
        [DataMember]
        public string Type_device { get; set; }
    }
}