﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfProjectAtlantis
{
    [DataContract]
    public class Datas
    {
        [DataMember]
        public string Id_data { get; set; }
        [DataMember]
        public string Date_data { get; set; }
        [DataMember]
        public string Value_data { get; set; }
        [DataMember]
        public string Unit_data { get; set; }
        [DataMember]
        public string Id_device { get; set; }
    }
}