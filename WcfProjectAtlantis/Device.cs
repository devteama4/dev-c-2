﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfProjectAtlantis
{
    [DataContract]
    public class Device
    {
        [DataMember]
        public string Type_device { get; set; }
        [DataMember]
        public string Value_TT { get; set; }
    }
}