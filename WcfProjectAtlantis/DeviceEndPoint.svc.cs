﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfProjectAtlantis
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "DeviceEndPoint" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez DeviceEndPoint.svc ou DeviceEndPoint.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class DeviceEndPoint : IDeviceEndPoint
    {
        private MySqlConnection connection;
        private MySqlDataReader reader;

        public DeviceEndPoint()
        {
            this.InitConnexion();
        }

        private void InitConnexion()
        {
            string connectionString = "SERVER=localhost;DATABASE=dbprojectatlantis;UID=root;PASSWORD=DBPWD;SslMode=None";
            this.connection = new MySqlConnection(connectionString);
        }
        public void InsertMetric(string datedata, string valuedata, string iddevice)
        {
            try
            {
                this.connection.Open();
                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "INSERT INTO data(date_data,value_data,id_device) VALUES (@date_data,@value_data,@id_device)";
                cmd.Parameters.AddWithValue("@date_data", datedata);
                cmd.Parameters.AddWithValue("@value_data", valuedata);
                cmd.Parameters.AddWithValue("@id_device", iddevice);

                cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }
        public List<Otherdevice> SendCmd()
        {
            List<Otherdevice> otherdevices = new List<Otherdevice>();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM other_device";

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Otherdevice otherdevice = new Otherdevice()
                    {
                        Id_other_device = reader[0].ToString(),
                        Type_other_device = reader[1].ToString(),
                        Condition_other_device = reader[2].ToString()
                    };
                    otherdevices.Add(otherdevice);
                }


                return otherdevices;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }

        }
    }
}
