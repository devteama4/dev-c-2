﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfProjectAtlantis
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "ICalculator" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface ICalculator
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            UriTemplate = "/GetData/{typedevice}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]

        List<Data> GetData(string typedevice);
        [OperationContract]
        [WebInvoke(
           Method = "GET",
           UriTemplate = "/GetDataDeviceDate/{typedevice}/{datedata}",
           BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json)]
        List<Data> GetDataDeviceDate(string typedevice, string datedata);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            UriTemplate = "/GetDataAll",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]

        List<Datas> GetDataAll();
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            UriTemplate = "/GetDataSum/{d}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Device GetDataSum(string d);
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            UriTemplate = "/GetDataBetDate/{firstdate}/{seconddate}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        List<Datas> GetDataBetDate(string firstdate, string seconddate);
    }
}
