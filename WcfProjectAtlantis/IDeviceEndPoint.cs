﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfProjectAtlantis
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IDeviceEndPoint" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IDeviceEndPoint
    {
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "/InsertMetric/{datedata}/{valuedata}/{iddevice}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void InsertMetric(string datedata, string valuedata, string iddevice);
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            UriTemplate = "/SendCmd",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        List<Otherdevice> SendCmd();
    }
}
