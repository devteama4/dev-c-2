﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfProjectAtlantis
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IMobileEndPoint" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IMobileEndPoint
    {
        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            UriTemplate = "/GetCmd/{typeotherdevice}/{conditionotherdevice}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        void GetCmd(string typeotherdevice, string conditionotherdevice);
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            UriTemplate = "/GetUser/{t}",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        List<User> GetUser(string t);
    }
}
