﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace WcfProjectAtlantis
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "MobileEndPoint" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez MobileEndPoint.svc ou MobileEndPoint.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class MobileEndPoint : IMobileEndPoint
    {
        private MySqlConnection connection;
        private MySqlDataReader reader;

        public MobileEndPoint()
        {
            this.InitConnexion();
        }

        private void InitConnexion()
        {
            string connectionString = "SERVER=localhost;DATABASE=dbprojectatlantis;UID=root;PASSWORD=DBPWD;SslMode=None";
            this.connection = new MySqlConnection(connectionString);
        }
        public void GetCmd(string typeotherdevice, string conditionotherdevice)
        {
            try
            {
                this.connection.Open();
                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "UPDATE other_device SET condition_other_device = @condition_other_device WHERE type_other_device = @type_other_device";
                cmd.Parameters.AddWithValue("@type_other_device", typeotherdevice);
                cmd.Parameters.AddWithValue("@condition_other_device", conditionotherdevice);

                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }

        public List<User> GetUser(string t)
        {
            List<User> users = new List<User>();
            try
            {
                this.connection.Open();

                MySqlCommand cmd = this.connection.CreateCommand();
                cmd.CommandText = "SELECT lastname_user,first_name_user,token_user,adress_mac_device,type_device FROM user INNER JOIN device ON user.id_user = device.id_user WHERE token_user = @token_user";
                cmd.Parameters.AddWithValue("@token_user", t);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    User user = new User() {
                        Lastname_user = reader[0].ToString(),
                        First_name_user = reader[1].ToString(),
                        Token_user = reader[2].ToString(),
                        Adress_mac_device = reader[3].ToString(),
                        Type_device = reader[4].ToString()
                    };
                    users.Add(user);
                }
                return users;
            }
            catch
            {
                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }
    }
}
