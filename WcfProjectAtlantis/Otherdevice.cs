﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfProjectAtlantis
{
    [DataContract]
    public class Otherdevice
    {
        [DataMember]
        public string Id_other_device { get; set; }
        [DataMember]
        public string Type_other_device { get; set; }
        [DataMember]
        public string Condition_other_device { get; set; }
    }
}