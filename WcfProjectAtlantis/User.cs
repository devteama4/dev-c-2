﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfProjectAtlantis
{
    [DataContract]
    public class User
    {
        [DataMember]
        public string Lastname_user { get; set; }
        [DataMember]
        public string First_name_user { get; set; }
        [DataMember]
        public string Token_user { get; set; }
        [DataMember]
        public string Adress_mac_device { get; set; }
        [DataMember]
        public string Type_device { get; set; }
    }
}